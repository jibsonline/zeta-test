resource "aws_route53_zone" "private" {
  name = "${local.kubernetes_cluster_name}"
  vpc {
    vpc_id = "${module.dev_vpc.vpc_id}"
  }
}
