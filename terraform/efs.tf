resource "aws_security_group" "ingress-efs" {
   name = "ingress-efs-sg"
   vpc_id = "${module.dev_vpc.vpc_id}"

   // NFS
   ingress {
     cidr_blocks = ["${module.dev_vpc.private_subnets_cidr_blocks}"]
     from_port = 2049
     to_port = 2049
     protocol = "tcp"
   }

   // Terraform removes the default rule
   egress {
     cidr_blocks = ["${module.dev_vpc.private_subnets_cidr_blocks}"]
     from_port = 0
     to_port = 0
     protocol = "-1"
   }
 }


resource "aws_efs_file_system" "efs-wordpress" {
   creation_token = "efs-wordpress"
   performance_mode = "generalPurpose"
   throughput_mode = "bursting"
 tags = {
     Name = "Wordpress"
   }
 }

resource "aws_efs_mount_target" "efs-mt-wordpress" {
   count = "${length(module.dev_vpc.private_subnets)}"
   file_system_id  = "${aws_efs_file_system.efs-wordpress.id}"
   subnet_id = "${module.dev_vpc.private_subnets[count.index]}"
   security_groups = ["${aws_security_group.ingress-efs.id}"]
 }
