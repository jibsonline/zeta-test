provider "aws" {
  region = "eu-west-2"
}

terraform {
  backend "s3" {
    encrypt = true
    bucket = "jibs-zeta-terraform"
    key    = "dev"
    region = "eu-west-2"
    dynamodb_table = "zeta-terraform"
  }
}

locals {
  azs                    = ["eu-west-2a", "eu-west-2b", "eu-west-2c"]
  environment            = "dev"
  kops_state_bucket_name = "jibs-zeta-terraform"
  // Needs to be a FQDN
  kubernetes_cluster_name = "k8s-dev0.wp.zeta"
  ingress_ips             = ["10.0.0.100/32", "10.0.0.101/32"]
  vpc_name                = "${local.environment}-vpc"

  tags = {
    environment = "${local.environment}"
    terraform   = true
  }
}

data "aws_region" "current" {}
